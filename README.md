# Przygotowanie

Aby uruchomić NBB należy przygotować schemat w bazie mySQL.
Nazwa schematu: newbuy
Schemat powinien być pusty – aplikacja sama stworzy potrzebne tabele.
Dodatkowo należy stworzyć użytkownika posiadającego pełne uprawnienia do schematu newbuy.
Nazwa użytkownika: newbuy
Hasło: sami

Aplikację należy uruchamiać na serwerze Tomcat. Gotowe środowisko (oparte na Tomcat 8.5.32): https://drive.google.com/drive/folders/17D5oLM6rvXcP0qx5m5X5m2QNvzxgt9M4

# Odpalenie serwera

Aby uruchomić serwer należy skrypt RUN_SERVER.cmd w lokalizacji NBB_package/apache-tomcat-8.5.32/bin

Po uruchomieniu w konsoli powinno pojawić się:

![alt text](doc/screenshots/console.png)

Następnie należy przejść do uruchomienia NBF.

# Konfiguracja IDE

Do rozwijania aplikacji NBB polecam IntellIJ IDEA (https://www.jetbrains.com/idea/). Powinna wystarczyć darmowa licencja. Gdyby jedna była potrzebna pełna wersja to możliwe jest uzyskanie jej poprzez stworzenie konta z maila Politechniki Śląskiej.
NBB należy uruchamiać jako aplikację Spring Boot.

![alt text](doc/screenshots/config.png)

![alt text](doc/screenshots/idea-tomcat.png)

Po przygotowaniu poprawek należy przygotować osobny war deplowoany na tomcat (niezależny od IntelliJ IDEA). 

![alt text](doc/screenshots/war-config.png)

![alt text](doc/screenshots/war-console.png)

Stworzona paczka NBB.war znajduje się w NBB-package/NBB/target.
Gotową paczkę należy przekopiować do webapps w katalogu tomcata. Kopiowanie te można wykonać za pomocą COPY WAR.cm Tomcat automatycznie zdeployuje aplikację z webapps.
