package pl.newbuy.util;

public enum ResponseKind {
    SUCCESS, ERROR, OCCUPIED_USERNAME
}