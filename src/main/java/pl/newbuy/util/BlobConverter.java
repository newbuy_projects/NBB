package pl.newbuy.util;

import org.apache.tomcat.util.codec.binary.Base64;
import javax.sql.rowset.serial.SerialBlob;
import java.sql.Blob;

public class BlobConverter {

    public Blob base64ToBlob(String img64) {
        try {
            byte[] img64bytes = img64.getBytes();
            byte[] img = Base64.decodeBase64(img64bytes);
            return new SerialBlob(img);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String blobToBase64(Blob blob) {
        String photo64 = "";
        try {
            int blobLength = (int) blob.length();
            byte[] blobAsBytes = blob.getBytes(1, blobLength);
            byte[] img64 = Base64.encodeBase64(blobAsBytes);
            photo64 = new String(img64);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return photo64;
    }
}
