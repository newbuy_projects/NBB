package pl.newbuy.util;

import pl.newbuy.dto.DateRangeDto;
import java.util.Calendar;
import java.util.Date;

public class DateRangeHelper {

    public boolean isInDateRange(DateRangeDto dateRangeDto, Date date) {
        return date == null ||
            dateRangeDto.getDateFrom() == null && dateRangeDto.getDateTo() == null ||
            dateRangeDto.getDateFrom() != null && dateRangeDto.getDateTo() == null && dateRangeDto.getDateFrom().before(date) ||
            dateRangeDto.getDateFrom() == null && dateRangeDto.getDateTo() != null && dateRangeDto.getDateTo().after(date) ||
            dateRangeDto.getDateFrom().before(date) && dateRangeDto.getDateTo().after(date);
    }

    public Date dateWithTime(Date date, int hour, int minute, int second, int millisecond){
        Calendar dateFromCalendar = Calendar.getInstance();
        dateFromCalendar.setTime(date);
        dateFromCalendar.set(Calendar.HOUR_OF_DAY, hour);
        dateFromCalendar.set(Calendar.MINUTE, minute);
        dateFromCalendar.set(Calendar.SECOND, second);
        dateFromCalendar.set(Calendar.MILLISECOND, millisecond);
        return dateFromCalendar.getTime();
    }

    public DateRangeDto updateDateRange(DateRangeDto dateRangeDto){
        if(dateRangeDto.getDateFrom() != null)
            dateRangeDto.setDateFrom(dateWithTime(dateRangeDto.getDateFrom(), 0, 0, 0, 0));
        if(dateRangeDto.getDateTo() != null)
            dateRangeDto.setDateTo(dateWithTime(dateRangeDto.getDateTo(), 23, 59, 59, 999));
        return dateRangeDto;
    }
}
