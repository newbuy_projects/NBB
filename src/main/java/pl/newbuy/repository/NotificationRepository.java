package pl.newbuy.repository;

import org.springframework.data.repository.CrudRepository;
import pl.newbuy.domain.Notification;

public interface NotificationRepository extends CrudRepository<Notification, Long> {

}
