package pl.newbuy.repository;

import org.springframework.data.repository.CrudRepository;
import pl.newbuy.domain.OrderItem;

public interface OrderItemRepository extends CrudRepository<OrderItem, Long> {

    Iterable<OrderItem> findByCartId(Long userId);
}
