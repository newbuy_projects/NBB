package pl.newbuy.repository;

import org.springframework.data.repository.CrudRepository;
import pl.newbuy.domain.Purchase;
import java.util.List;

public interface PurchaseRepository extends CrudRepository<Purchase, Long> {

    List<Purchase> findByProductId(Long id);

    List<Purchase> findByProductIdOrderByDateAsc(Long productId);
}
