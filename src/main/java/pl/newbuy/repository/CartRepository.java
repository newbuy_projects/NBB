package pl.newbuy.repository;

import org.springframework.data.repository.CrudRepository;
import pl.newbuy.domain.Cart;
import java.util.List;

public interface CartRepository extends CrudRepository<Cart, Long> {

    List<Cart> findByUserIdAndStatus(Long id, int status);
    List<Cart> findByStatus(int status);
}
