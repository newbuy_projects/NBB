package pl.newbuy.repository;

import org.springframework.data.repository.CrudRepository;
import pl.newbuy.domain.Category;

public interface CategoryRepository extends CrudRepository<Category, Long> {

}
