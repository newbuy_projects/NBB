package pl.newbuy.repository;

import org.springframework.data.repository.CrudRepository;
import pl.newbuy.domain.Photo;

import java.util.List;

public interface PhotoRepository extends CrudRepository<Photo, Long> {

    List<Photo> findByProductId(Long productId);
}
