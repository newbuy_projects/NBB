package pl.newbuy.repository;

import org.springframework.data.repository.CrudRepository;
import pl.newbuy.domain.Product;
import pl.newbuy.domain.Rate;
import pl.newbuy.domain.User;

import java.util.List;

public interface RateRepository extends CrudRepository<Rate, Long> {

    List<Rate> findByProductId(Long productId);
    List<Rate> findByUserAndProduct(User user, Product product);
}
