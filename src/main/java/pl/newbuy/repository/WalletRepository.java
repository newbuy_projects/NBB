package pl.newbuy.repository;

import org.springframework.data.repository.CrudRepository;
import pl.newbuy.domain.Wallet;
import java.util.List;

public interface WalletRepository extends CrudRepository<Wallet, Long> {

    List<Wallet> findByUserId(Long userId);
}
