package pl.newbuy.config.security.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import pl.newbuy.config.security.model.AuthorizationToken;
import pl.newbuy.domain.User;
import pl.newbuy.service.UserService;
import java.io.Serializable;
import java.util.Date;
import java.util.function.Function;
import static pl.newbuy.config.security.Constants.ACCESS_TOKEN_VALIDITY_SECONDS;
import static pl.newbuy.config.security.Constants.SIGNING_KEY;

@Component
public class JwtTokenUtil implements Serializable {

    @Autowired
    private UserService userService;

    public String getUsernameFromToken(AuthorizationToken token) {
        return getClaimFromToken(token, Claims::getSubject);
    }

    public Date getExpirationDateFromToken(AuthorizationToken token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    public <T> T getClaimFromToken(AuthorizationToken token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    private Claims getAllClaimsFromToken(AuthorizationToken token) {
        return Jwts.parser()
                .setSigningKey(SIGNING_KEY)
                .parseClaimsJws(token.getToken())
                .getBody();
    }

    private Boolean isTokenExpired(AuthorizationToken token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    public String generateToken(User user) {
        Claims claims = Jwts.claims().setSubject(user.getUsername());
        claims.put("scopes", userService.getAuthority(user.getIsAdmin()));

        return Jwts.builder()
                .setClaims(claims)
                .setIssuer("http://newbuy.pl")
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + ACCESS_TOKEN_VALIDITY_SECONDS*1000))
                .signWith(SignatureAlgorithm.HS256, SIGNING_KEY)
                .compact();
    }

    public Boolean validateToken(AuthorizationToken token, UserDetails userDetails) {
        final String username = getUsernameFromToken(token);
        return (
              username.equals(userDetails.getUsername())
                    && !isTokenExpired(token));
    }

}
