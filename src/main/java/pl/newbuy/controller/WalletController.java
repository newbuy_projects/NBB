package pl.newbuy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.newbuy.domain.Wallet;
import pl.newbuy.dto.TopUpDto;
import pl.newbuy.repository.UserRepository;
import pl.newbuy.repository.WalletRepository;
import pl.newbuy.util.ResponseEntityHelper;

import java.util.Date;

@Controller
@RequestMapping(path = "/wallet")
public class WalletController {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private WalletRepository walletRepository;

    @GetMapping(path="")
    public @ResponseBody Wallet getWallet() {
        return userRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).getWallet();
    }

    @PostMapping(path="/topUp")
    @PreAuthorize("hasRole('ADMIN')")
    public @ResponseBody
    ResponseEntity<?> topUp(@RequestBody TopUpDto topUpDto) {
        Wallet wallet = userRepository.findOne(topUpDto.getUserId()).getWallet();
        wallet.setBalance(wallet.getBalance() + topUpDto.getAmount());
        wallet.setTopUpDate(new Date());
        walletRepository.save(wallet);
        return ResponseEntityHelper.jsonOkResponse();
    }
}
