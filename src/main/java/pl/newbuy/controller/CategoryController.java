package pl.newbuy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.newbuy.domain.Category;
import pl.newbuy.domain.Product;
import pl.newbuy.repository.CategoryRepository;
import pl.newbuy.repository.ProductRepository;
import pl.newbuy.util.ResponseEntityHelper;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Controller
@RequestMapping(path="/category")
public class CategoryController {
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private ProductRepository productRepository;

    @GetMapping(path="")
    public @ResponseBody Iterable<Category> getCategories() {
        return StreamSupport.stream(categoryRepository.findAll().spliterator(), false)
                .filter(Category::getVisibility)
                .collect(Collectors.toList());
    }

    @GetMapping(path="/all")
    @PreAuthorize("hasRole('ADMIN')")
    public @ResponseBody Iterable<Category> getAllCategories() {
        return categoryRepository.findAll();
    }

    @GetMapping(path="/{categoryId}")
    public @ResponseBody Category getCategory(@PathVariable(value="categoryId") Long categoryId) {
        Category category = categoryRepository.findOne(categoryId);
        if(category == null || !category.getVisibility())
            return null;
        return category;
    }

    @GetMapping(path="/products/{categoryId}")
    public @ResponseBody List<Product> getProductsFromCategory(@PathVariable(value="categoryId") Long categoryId) {
        return productRepository.findByCategoryId(categoryId).stream().filter(product -> product.getVisibility() && product.getCategory().getVisibility()).collect(Collectors.toList());
    }

    @PostMapping(path="")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> updateCategory (@RequestBody Category category) throws Exception {
        category.setVisibility(false);
        categoryRepository.save(category);
        return ResponseEntityHelper.jsonOkResponse();
    }

    @PutMapping(path="/{categoryId}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> updateProduct (@PathVariable(value="categoryId") Long categoryId, @RequestBody Category newCategoryData) throws Exception {
        Category category = categoryRepository.findOne(categoryId);
        if(category == null) throw new Exception("category not found");
        category.setPromotion(newCategoryData.getPromotion());
        category.setName(newCategoryData.getName());
        categoryRepository.save(category);
        return ResponseEntityHelper.jsonOkResponse();
    }

    @GetMapping(path="/show/{categoryId}")
    @PreAuthorize("hasRole('ADMIN')")
    public @ResponseBody ResponseEntity<?> showCategory(@PathVariable(value="categoryId") Long categoryId) throws Exception {
        Category category = categoryRepository.findOne(categoryId);
        if(category == null)
            throw new Exception("Category not found");
        category.setVisibility(true);
        categoryRepository.save(category);
        return ResponseEntityHelper.jsonOkResponse();
    }

    @GetMapping(path="/hide/{categoryId}")
    @PreAuthorize("hasRole('ADMIN')")
    public @ResponseBody ResponseEntity<?> hideCategory(@PathVariable(value="categoryId") Long categoryId) throws Exception {
        Category category = categoryRepository.findOne(categoryId);
        if(category == null)
            throw new Exception("Category not found");
        category.setVisibility(false);
        categoryRepository.save(category);
        return ResponseEntityHelper.jsonOkResponse();
    }
}
