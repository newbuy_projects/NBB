package pl.newbuy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.newbuy.domain.Category;
import pl.newbuy.domain.Product;
import pl.newbuy.repository.CategoryRepository;
import pl.newbuy.repository.ProductRepository;
import pl.newbuy.util.ResponseEntityHelper;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Controller
@RequestMapping(path="/product")
public class ProductController {
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private CategoryRepository categoryRepository;

    @PostMapping(path="")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> updateProduct (@RequestBody Product product) throws Exception {
        Category category = categoryRepository.findOne(product.getCategory().getId());
        if(category == null) throw new Exception("category not found");
        product.setAvailability(false);
        product.setVisibility(false);
        product.setQuantity(0);
        product.setCategory(category);
        productRepository.save(product);
        return ResponseEntityHelper.jsonOkResponse();
    }

    @PutMapping(path="/{productId}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> updateProduct (@PathVariable(value="productId") Long productId, @RequestBody Product newProductData) throws Exception {
        Category category = categoryRepository.findOne(newProductData.getCategory().getId());
        if(category == null) throw new Exception("category not found");
        Product product = productRepository.findOne(productId);
        if(product == null) throw new Exception("product not found");
        product.setCategory(category);
        product.setAmount(newProductData.getAmount());
        product.setDescription(newProductData.getDescription());
        product.setName(newProductData.getName());
        productRepository.save(product);
        return ResponseEntityHelper.jsonOkResponse();
    }

    @GetMapping(path="/{productId}")
    public @ResponseBody Product getProduct(@PathVariable(value="productId") Long productId) {
        Product product = productRepository.findOne(productId);
        if(product == null || !product.getVisibility())
            return null;
        return product;
    }

    @GetMapping(path="/enable/{productId}")
    @PreAuthorize("hasRole('ADMIN')")
    public @ResponseBody ResponseEntity<?> enableProduct(@PathVariable(value="productId") Long productId) throws Exception {
        Product product = productRepository.findOne(productId);
        if(product == null)
            throw new Exception("Product not found");
        product.setAvailability(true);
        productRepository.save(product);
        return ResponseEntityHelper.jsonOkResponse();
    }

    @GetMapping(path="/disable/{productId}")
    @PreAuthorize("hasRole('ADMIN')")
    public @ResponseBody ResponseEntity<?> disableProduct(@PathVariable(value="productId") Long productId) throws Exception {
        Product product = productRepository.findOne(productId);
        if(product == null)
            throw new Exception("Product not found");
        product.setAvailability(false);
        productRepository.save(product);
        return ResponseEntityHelper.jsonOkResponse();
    }

    @GetMapping(path="/show/{productId}")
    @PreAuthorize("hasRole('ADMIN')")
    public @ResponseBody ResponseEntity<?> showProduct(@PathVariable(value="productId") Long productId) throws Exception {
        Product product = productRepository.findOne(productId);
        if(product == null)
            throw new Exception("Product not found");
        product.setVisibility(true);
        productRepository.save(product);
        return ResponseEntityHelper.jsonOkResponse();
    }

    @GetMapping(path="/hide/{productId}")
    @PreAuthorize("hasRole('ADMIN')")
    public @ResponseBody ResponseEntity<?> hideProduct(@PathVariable(value="productId") Long productId) throws Exception {
        Product product = productRepository.findOne(productId);
        if(product == null)
            throw new Exception("Product not found");
        product.setVisibility(false);
        productRepository.save(product);
        return ResponseEntityHelper.jsonOkResponse();
    }

    @GetMapping(path="")
    public @ResponseBody Iterable<Product> getProducts() {
        return StreamSupport.stream(productRepository.findAll().spliterator(), false)
                .filter(Product::getVisibility)
                .collect(Collectors.toList());
    }

    @GetMapping(path="/all")
    @PreAuthorize("hasRole('ADMIN')")
    public @ResponseBody Iterable<Product> getAllProducts() {
        return productRepository.findAll();
    }
}
