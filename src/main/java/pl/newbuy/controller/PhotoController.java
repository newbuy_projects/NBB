package pl.newbuy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.newbuy.domain.Photo;
import pl.newbuy.domain.Product;
import pl.newbuy.dto.PhotoDto;
import pl.newbuy.repository.PhotoRepository;
import pl.newbuy.repository.ProductRepository;
import pl.newbuy.util.BlobConverter;
import pl.newbuy.util.ResponseEntityHelper;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Controller
@RequestMapping(path="/photo")
public class PhotoController {
    @Autowired
    private PhotoRepository photoRepository;
    @Autowired
    private ProductRepository productRepository;

    @GetMapping(path="/fromProduct/{productId}")
    public @ResponseBody List<PhotoDto> getPhotoFromProduct(@PathVariable(value="productId") Long productId) {
        return photoRepository.findByProductId(productId).stream().map(photo -> {
            PhotoDto photoDto = new PhotoDto();
            photoDto.setId(photo.getId());
            photoDto.setContent(new BlobConverter().blobToBase64(photo.getPhoto()));
            photoDto.setProductId(photo.getProduct().getId());
            return photoDto;
        }).collect(Collectors.toCollection(ArrayList::new));
    }

    @GetMapping(path="/firstPhoto/{categoryId}")
    public @ResponseBody List<PhotoDto> getFirstPhotos(@PathVariable(value="categoryId") Long categoryId) {
        List<Product> products;
        if(categoryId == -1)
            products = StreamSupport.stream(productRepository.findAll().spliterator(), false).collect(Collectors.toList());
        else
            products = productRepository.findByCategoryId(categoryId);
        List<PhotoDto> photoList = new ArrayList<>();
        products.forEach(product -> {
            List<Photo> photos = photoRepository.findByProductId(product.getId());
            if(!photos.isEmpty()) {
                Photo photo = photos.get(0);
                PhotoDto photoDto = new PhotoDto();
                photoDto.setId(photo.getId());
                photoDto.setContent(new BlobConverter().blobToBase64(photo.getPhoto()));
                photoDto.setProductId(photo.getProduct().getId());
                photoList.add(photoDto);
            }
        });
        return photoList;
    }

    @DeleteMapping(path="/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public @ResponseBody
    ResponseEntity<?> deletePhoto(@PathVariable(value="id") Long id){
        photoRepository.delete(id);
        return ResponseEntityHelper.jsonOkResponse();
    }

    @PostMapping(path="")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> addPhoto (@RequestBody PhotoDto photoDto) throws Exception {
        Product product = productRepository.findOne(photoDto.getProductId());
        if(product == null) throw new Exception("product not found");
        Photo photo = new Photo();
        photo.setProduct(product);
        photo.setPhoto(new BlobConverter().base64ToBlob(photoDto.getContent()));
        photoRepository.save(photo);
        return ResponseEntityHelper.jsonOkResponse();
    }
}
