package pl.newbuy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.newbuy.domain.User;
import pl.newbuy.dto.ShortUserDto;
import pl.newbuy.dto.UserDetailsDto;
import pl.newbuy.service.UserService;
import pl.newbuy.util.ResponseEntityHelper;
import pl.newbuy.repository.UserRepository;
import pl.newbuy.util.ResponseKind;

@Controller
@RequestMapping(path="/user")
public class UserController {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserService userService;

    @GetMapping(path = "")
    @PreAuthorize("hasRole('ADMIN')")
    public @ResponseBody
    Iterable<User> getAllUsers() {
        return userRepository.findAll();
    }

    @GetMapping(path = "/current")
    public @ResponseBody
    ShortUserDto getCurrentUser() throws Exception {
        User user = userRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        if(user == null) throw new Exception("user not found");
        ShortUserDto shortUserDto = new ShortUserDto();
        shortUserDto.setId(user.getId());
        shortUserDto.setAdmin(user.getIsAdmin());
        shortUserDto.setUsername(user.getUsername());
        return shortUserDto;
    }

    @GetMapping(path = "/details")
    public @ResponseBody
    UserDetailsDto getUserDetails() throws Exception {
        User user = userRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        if(user == null) throw new Exception("user not found");
        UserDetailsDto userDetailsDto = new UserDetailsDto();
        userDetailsDto.setUsername(user.getUsername());
        userDetailsDto.setFirstname(user.getFirstname());
        userDetailsDto.setMail(user.getMail());
        userDetailsDto.setSurname(user.getSurname());
        userDetailsDto.setPhone(user.getPhone());
        return userDetailsDto;
    }

    @RequestMapping(path = "/register", method = RequestMethod.POST)
    public ResponseEntity<?> registerUser (@RequestBody UserDetailsDto userDetailsDto) {
        if(userRepository.findByUsername(userDetailsDto.getUsername()) != null)
            return ResponseEntityHelper.jsonCodeResponse(ResponseKind.OCCUPIED_USERNAME);
        userService.register(userDetailsDto);
        return ResponseEntityHelper.jsonCodeResponse(ResponseKind.SUCCESS);
    }

    @RequestMapping(path = "/update", method = RequestMethod.POST)
    public ResponseEntity<?> updateUser (@RequestBody UserDetailsDto userDetailsDto) throws Exception {
        if(!userDetailsDto.getUsername().equals(SecurityContextHolder.getContext().getAuthentication().getName()))
            throw new Exception("wrong user");
        User user = userRepository.findByUsername(userDetailsDto.getUsername());
        if(user == null)
            throw new Exception("user not found");
        userService.update(user, userDetailsDto);
        return ResponseEntityHelper.jsonOkResponse();
    }
}
