package pl.newbuy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.newbuy.domain.*;
import pl.newbuy.dto.CartDto;
import pl.newbuy.dto.CartItemDto;
import pl.newbuy.dto.OrderDto;
import pl.newbuy.service.CartService;
import pl.newbuy.service.NotificationService;
import pl.newbuy.util.ResponseEntityHelper;
import pl.newbuy.repository.CartRepository;
import pl.newbuy.repository.OrderItemRepository;
import pl.newbuy.repository.ProductRepository;
import pl.newbuy.repository.UserRepository;
import java.util.Date;
import java.util.List;
import static java.lang.Math.toIntExact;

@Controller
@RequestMapping(path = "/cart")
public class CartController {
    @Autowired
    private CartRepository cartRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private OrderItemRepository orderItemRepository;
    @Autowired
    private CartService cartService;
    @Autowired
    private NotificationService notificationService;

    @GetMapping(path = "")
    public @ResponseBody
    CartDto getCart() throws Exception {
        List<Cart> cartList = cartRepository.findByUserIdAndStatus(userRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).getId(), 0);
        if (cartList.isEmpty()) throw new Exception("cart not found");
        return cartService.createCartDto(cartList.get(0));
    }

    @GetMapping(path="/{id}")
    public @ResponseBody CartDto getCartById(@PathVariable(value="id") Long id) throws Exception {
        Cart cart = cartRepository.findOne(id);
        if (cart == null)
            throw new Exception("cart not found");
        if (!cart.getUser().getUsername().equals(SecurityContextHolder.getContext().getAuthentication().getName())
                && !userRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).getIsAdmin())
            throw new Exception("wrong user");
        return cartService.createCartDto(cart);
    }

    @DeleteMapping(path="/{id}")
    public @ResponseBody ResponseEntity<?> deleteFromCart(@PathVariable(value="id") Long id) throws Exception  {
        OrderItem orderItem = orderItemRepository.findOne(id);
        if (orderItem == null)
            throw new Exception("orderItem not found");
        if (!orderItem.getCart().getUser().getUsername().equals(SecurityContextHolder.getContext().getAuthentication().getName()))
            throw new Exception("wrong user");
        Product product = productRepository.findOne(orderItem.getPurchase().getProduct().getId());
        product.setQuantity(product.getQuantity() + orderItem.getQuantity());
        productRepository.save(product);
        orderItemRepository.delete(id);
        return ResponseEntityHelper.jsonOkResponse();
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<?> addItemToCart(@RequestBody CartItemDto cartItemDto) throws Exception {
        User user = userRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        if (user == null) throw new Exception("user not found");
        List<Cart> cartList = cartRepository.findByUserIdAndStatus(user.getId(), 0);
        if (cartList.isEmpty()) throw new Exception("cart not found");
        Product product = productRepository.findOne(cartItemDto.getProductId());
        if (product == null) throw new Exception("product not found");
        if(product.getQuantity() < cartItemDto.getQuantity()) throw new Exception("Invalid quantity");
        List<OrderItem> orderItems = cartService.createOrderItems(cartItemDto, product, cartList.get(0));
        product.setQuantity(product.getQuantity() - cartItemDto.getQuantity());
        if(product.getQuantity() < 1) product.setAvailability(false);
        productRepository.save(product);
        orderItemRepository.save(orderItems);
        return ResponseEntityHelper.jsonOkResponse();
    }

    @GetMapping(path = "/order")
    public @ResponseBody ResponseEntity<?> order() throws Exception {
        User user = userRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        if (user == null) throw new Exception("user not found");
        List<Cart> cartList = cartRepository.findByUserIdAndStatus(user.getId(), 0);
        if (cartList.isEmpty()) throw new Exception("cart not found");
        Cart cart = cartList.get(0);
        if(cart.getOrders().isEmpty()) throw new Exception("empty order list");
        int totalAmount = toIntExact(Math.round(cart.getOrders().stream().mapToDouble(order -> order.getQuantity() * order.getAmount()).sum()));
        Wallet wallet = user.getWallet();
        if(totalAmount > wallet.getBalance()) throw new Exception("wrong balance");
        wallet.setBalance(wallet.getBalance() - totalAmount);
        cart.setStatus(1);
        cart.setOrderDate(new Date());
        Cart newCart = new Cart();
        newCart.setStatus(0);
        newCart.setUser(user);
        cartRepository.save(cart);
        cartRepository.save(newCart);
        userRepository.save(user);
        notificationService.createFromOrder(cart, totalAmount);
        return ResponseEntityHelper.jsonOkResponse();
    }

    @GetMapping(path = "/ordered")
    public @ResponseBody List<OrderDto> getOrders () throws Exception {
        User user = userRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        if (user == null) throw new Exception("user not found");
        List<Cart> cartList = cartRepository.findByUserIdAndStatus(user.getId(), 1);
        return cartService.toOrderDtos(cartList);
    }

    @GetMapping(path = "/ordered/all")
    public @ResponseBody List<OrderDto> getAllOrders () throws Exception {
        User user = userRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        if (user == null) throw new Exception("user not found");
        List<Cart> cartList = cartRepository.findByStatus(1);
        return cartService.toOrderDtos(cartList);
    }
}
