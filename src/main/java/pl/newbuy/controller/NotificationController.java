package pl.newbuy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.newbuy.domain.Notification;
import pl.newbuy.dto.MessageDto;
import pl.newbuy.service.NotificationService;
import pl.newbuy.util.ResponseEntityHelper;
import pl.newbuy.util.ResponseKind;

@Controller
@RequestMapping(path="/notification")
public class NotificationController {

    @Autowired
    private NotificationService notificationService;

    @GetMapping(path="")
    @PreAuthorize("hasRole('ADMIN')")
    public @ResponseBody Iterable<Notification> getAllNotifications() {
        return notificationService.getAllNotifications();
    }

    @RequestMapping(path = "/message", method = RequestMethod.POST)
    public ResponseEntity<?> newMessage (@RequestBody MessageDto messageDto) {
        notificationService.createFromMessage(messageDto);
        return ResponseEntityHelper.jsonCodeResponse(ResponseKind.SUCCESS);
    }
}
