package pl.newbuy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.newbuy.domain.OrderItem;
import pl.newbuy.domain.Purchase;
import pl.newbuy.dto.DateRangeDto;
import pl.newbuy.dto.ReportFilterDto;
import pl.newbuy.dto.ReportItemDto;
import pl.newbuy.repository.OrderItemRepository;
import pl.newbuy.repository.ProductRepository;
import pl.newbuy.repository.PurchaseRepository;
import pl.newbuy.util.DateRangeHelper;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import static java.lang.Math.toIntExact;

@Controller
@RequestMapping(path = "/report")
public class ReportController {
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private PurchaseRepository purchaseRepository;
    @Autowired
    private OrderItemRepository orderItemRepository;

    @PostMapping(path="")
    public @ResponseBody
    List<ReportItemDto> getReportItems(@RequestBody ReportFilterDto reportFilterDto) {
        DateRangeHelper dateRangeHelper = new DateRangeHelper();
        DateRangeDto date = dateRangeHelper.updateDateRange(reportFilterDto.getDateRangeDto());
        return StreamSupport.stream(orderItemRepository.findAll().spliterator(), false)
            .filter(orderItem -> dateRangeHelper.isInDateRange(date, orderItem.getPurchase().getDate()))
            .filter(orderItem -> reportFilterDto.getProductId() == 0 || orderItem.getPurchase().getProduct().getId() == reportFilterDto.getProductId())
            .collect(Collectors.groupingBy(OrderItem::getPurchase, Collectors.toList()))
            .entrySet().stream().map(entry -> {
                List<OrderItem> orderItems = entry.getValue();
                Purchase purchase = entry.getKey();
                ReportItemDto reportItemDto = new ReportItemDto();
                reportItemDto.setProductId(purchase.getProduct().getId());
                reportItemDto.setProductName(purchase.getProduct().getName());
                reportItemDto.setDate(purchase.getDate());
                reportItemDto.setSoldTotalAmount(toIntExact(Math.round(orderItems.stream()
                .mapToDouble(orderItem -> orderItem.getQuantity() * orderItem.getAmount()).sum())));
                reportItemDto.setSoldQuantity(toIntExact(Math.round(orderItems.stream()
                .mapToDouble(OrderItem::getQuantity).sum())));
                reportItemDto.setPurchasedAmount(purchase.getAmount());
                return reportItemDto;
            }).collect(Collectors.toList());
    }
}
