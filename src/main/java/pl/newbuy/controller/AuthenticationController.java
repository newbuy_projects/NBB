package pl.newbuy.controller;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import pl.newbuy.config.security.model.AuthorizationToken;
import pl.newbuy.config.security.util.JwtTokenUtil;
import pl.newbuy.domain.User;
import pl.newbuy.dto.LoginUserDto;
import pl.newbuy.service.UserService;
import pl.newbuy.util.ResponseEntityHelper;

@RestController
@RequestMapping("/token")
public class AuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/invalidate", method = RequestMethod.GET)
    public String invalidate(){
        SecurityContextHolder.getContext().setAuthentication(null);
        return "logged out";
    }

    @RequestMapping(value = "/check", method = RequestMethod.GET, produces="text/plain")
    public String check(){
        return "valid";
    }

    @RequestMapping(value = "/generate-token", method = RequestMethod.POST)
    public ResponseEntity<?> register(@RequestBody LoginUserDto loginUserDto) throws AuthenticationException {
        String password = new String(Base64.decodeBase64(loginUserDto.getPassword()));
        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginUserDto.getUsername(),
                        password
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        final User user = userService.findOne(loginUserDto.getUsername());
        final String token = jwtTokenUtil.generateToken(user);
        final AuthorizationToken authorizationToken = new AuthorizationToken(token);
        return ResponseEntityHelper.jsonTokenResponse(authorizationToken.getToken());
    }
}
