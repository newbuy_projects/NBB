package pl.newbuy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.newbuy.domain.*;
import pl.newbuy.dto.CommentDto;
import pl.newbuy.dto.NewCommentDto;
import pl.newbuy.service.NotificationService;
import pl.newbuy.util.ResponseEntityHelper;
import pl.newbuy.repository.CommentRepository;
import pl.newbuy.repository.ProductRepository;
import pl.newbuy.repository.UserRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping(path="/comment")
public class CommentController {
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private NotificationService notificationService;

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<?> addComment(@RequestBody NewCommentDto newCommentDto) throws Exception {
        User user = userRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        if (user == null) throw new Exception("user not found");
        Product product = productRepository.findOne(newCommentDto.getProductId());
        if (product == null) throw new Exception("product not found");
        Comment comment = new Comment();
        comment.setContent(newCommentDto.getContent());
        comment.setUser(user);
        comment.setProduct(product);
        commentRepository.save(comment);
        return ResponseEntityHelper.jsonIdResponse(comment.getId());
    }

    @GetMapping(path="/fromProduct/{productId}")
    public @ResponseBody List<CommentDto> getCommentsFromProduct(@PathVariable(value="productId") Long productId) {
        return commentRepository.findByProductId(productId).stream()
                .map(comment -> new CommentDto(comment.getId(), comment.getContent(), comment.getUser().getUsername()))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    @RequestMapping(path = "/{commentId}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteComment(@PathVariable(value="commentId") Long commentId) throws Exception {
        User user = userRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        if (user == null) throw new Exception("user not found");
        Comment comment = commentRepository.findOne(commentId);
        if (comment == null) throw new Exception("comment not found");
        if (comment.getUser() != user && !user.getIsAdmin()) throw new Exception("wrong user");
        notificationService.createDeletedComment(comment, user);
        commentRepository.delete(commentId);
        return ResponseEntityHelper.jsonOkResponse();
    }
}
