package pl.newbuy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.newbuy.domain.Product;
import pl.newbuy.domain.Purchase;
import pl.newbuy.dto.PurchaseDto;
import pl.newbuy.repository.ProductRepository;
import pl.newbuy.repository.PurchaseRepository;
import pl.newbuy.service.PurchaseService;
import pl.newbuy.util.ResponseEntityHelper;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Controller
@RequestMapping(path="/purchase")
public class PurchaseController {
    @Autowired
    private PurchaseRepository purchaseRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private PurchaseService purchaseService;

    @GetMapping(path = "")
    @PreAuthorize("hasRole('ADMIN')")
    public @ResponseBody
    List<PurchaseDto> getPurchases(){
        return StreamSupport.stream(purchaseRepository.findAll().spliterator(), false)
                .map(purchase -> purchaseService.toPurchaseDto(purchase))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    @PostMapping(path = "")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> addPurchase(@RequestBody PurchaseDto purchaseDto) throws Exception {
        Product product = productRepository.findOne(purchaseDto.getProductId());
        if (product == null) throw new Exception("product not found");
        Purchase purchase = new Purchase();
        purchase.setProduct(product);
        purchase.setQuantity(purchaseDto.getQuantity());
        purchase.setAvailableQuantity(purchaseDto.getQuantity());
        purchase.setDate(new Date());
        purchase.setAmount(purchaseDto.getAmount());
        product.setQuantity(product.getQuantity() + purchase.getQuantity());
        purchaseRepository.save(purchase);
        productRepository.save(product);
        return ResponseEntityHelper.jsonOkResponse();
    }
}
