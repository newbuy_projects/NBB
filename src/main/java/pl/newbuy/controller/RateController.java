package pl.newbuy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.newbuy.domain.Product;
import pl.newbuy.domain.Rate;
import pl.newbuy.domain.User;
import pl.newbuy.dto.NewRateDto;
import pl.newbuy.dto.ProductRatesDto;
import pl.newbuy.repository.ProductRepository;
import pl.newbuy.repository.RateRepository;
import pl.newbuy.repository.UserRepository;
import pl.newbuy.util.ResponseEntityHelper;
import java.util.List;
import static java.lang.Math.toIntExact;

@Controller
@RequestMapping(path="/rate")
public class RateController {
    @Autowired
    private RateRepository rateRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private UserRepository userRepository;

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<?> addRate(@RequestBody NewRateDto newRateDto) throws Exception {
        User user = userRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        if (user == null) throw new Exception("user not found");
        Product product = productRepository.findOne(newRateDto.getProductId());
        if (product == null) throw new Exception("product not found");
        List<Rate> rateList = rateRepository.findByUserAndProduct(user, product);
        Rate rate = rateList.isEmpty() ? new Rate() : rateList.get(0);
        rate.setUser(user);
        rate.setProduct(product);
        rate.setRate(newRateDto.getRate());
        rateRepository.save(rate);
        return ResponseEntityHelper.jsonOkResponse();
    }

    @GetMapping(path="/fromProduct/{productId}")
    public @ResponseBody ProductRatesDto getRatesFromProduct(@PathVariable(value="productId") Long productId) {
        ProductRatesDto productRatesDto = new ProductRatesDto();
        List<Rate> rates = rateRepository.findByProductId(productId);
        productRatesDto.setQuantity(rates.size());
        productRatesDto.setAverageRate(toIntExact(Math.round(rates.stream().mapToDouble(Rate::getRate).average().orElse(0))));
        return productRatesDto;
    }
}
