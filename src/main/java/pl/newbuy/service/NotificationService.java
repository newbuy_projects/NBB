package pl.newbuy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.newbuy.domain.Cart;
import pl.newbuy.domain.Comment;
import pl.newbuy.domain.Notification;
import pl.newbuy.domain.User;
import pl.newbuy.dto.MessageDto;
import pl.newbuy.repository.NotificationRepository;
import java.util.Date;

@Component
public class NotificationService {
	
	@Autowired
	private NotificationRepository notificationRepository;

	public void newNotification(String content, Date date){
		Notification notification = new Notification();
		notification.setText(content);
		notification.setDate(date);
		notificationRepository.save(notification);
	}

	public void newNotification(String content){
		newNotification(content, new Date());
	}

	public void createFromMessage(MessageDto messageDto){
		newNotification("WIADOMOŚĆ # od: " + messageDto.getFirstname() + " " + messageDto.getSurname() + " " +
				messageDto.getMail() + " # " + messageDto.getContent());
	}

	public void createFromOrder(Cart cart, int totalAmount){
		newNotification("ZAMÓWIENIE # : " + cart.getUser().getFirstname() + " " + cart.getUser().getSurname() + " " +
				cart.getUser().getUsername() + " # " + cart.getId() + " # " + totalAmount + " zł");
	}

	public void createDeletedComment(Comment comment, User user){
		newNotification("USUNIĘCIE KOMENTARZA # : " + comment.getUser().getUsername() + " # " + comment.getProduct().getName() + " # " +
				comment.getContent() + " # " + user.getUsername());
	}

	public Iterable<Notification> getAllNotifications(){
		return notificationRepository.findAll();
	}
}
