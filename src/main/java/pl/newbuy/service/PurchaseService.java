package pl.newbuy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.newbuy.domain.Purchase;
import pl.newbuy.dto.PurchaseDto;
import pl.newbuy.dto.SelectedPurchaseDto;
import pl.newbuy.repository.PurchaseRepository;
import java.util.ArrayList;
import java.util.List;

@Component
public class PurchaseService {
	@Autowired
	private PurchaseRepository purchaseRepository;

	public PurchaseDto toPurchaseDto(Purchase purchase){
		PurchaseDto purchaseDto = new PurchaseDto();
		purchaseDto.setDate(purchase.getDate());
		purchaseDto.setId(purchase.getId());
		purchaseDto.setProductId(purchase.getProduct().getId());
		purchaseDto.setProductName(purchase.getProduct().getName());
		purchaseDto.setQuantity(purchase.getQuantity());
		purchaseDto.setAmount(purchase.getAmount());
		return purchaseDto;
	}

	public List<SelectedPurchaseDto> orderFromOldestPurchases(Long productId, int quantity){
		List<Purchase> purchases = purchaseRepository.findByProductIdOrderByDateAsc(productId);
		List<SelectedPurchaseDto> selectedPurchaseDtos = new ArrayList<>();
		for(Purchase purchase : purchases){
			if(quantity == 0) break;
			else{
				SelectedPurchaseDto selectedPurchaseDto = new SelectedPurchaseDto();
				selectedPurchaseDto.setPurchaseId(purchase.getId());
				int selectedQuantity = purchase.getAvailableQuantity() > quantity ? quantity : purchase.getAvailableQuantity();
				quantity -= selectedQuantity;
				selectedPurchaseDto.setQuantity(selectedQuantity);
				purchase.setAvailableQuantity(purchase.getAvailableQuantity() - selectedQuantity);
				selectedPurchaseDtos.add(selectedPurchaseDto);
			}
		}
		purchaseRepository.save(purchases);
		return selectedPurchaseDtos;
	}
}
