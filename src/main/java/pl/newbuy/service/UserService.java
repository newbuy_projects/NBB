package pl.newbuy.service;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import pl.newbuy.domain.Cart;
import pl.newbuy.domain.User;
import pl.newbuy.domain.Wallet;
import pl.newbuy.dto.UserDetailsDto;
import pl.newbuy.repository.CartRepository;
import pl.newbuy.repository.UserRepository;
import pl.newbuy.repository.WalletRepository;
import java.util.Arrays;
import java.util.List;

@Component
public class UserService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private CartRepository cartRepository;
	@Autowired
	private WalletRepository walletRepository;

	@Autowired
	private BCryptPasswordEncoder bcryptEncoder;

	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findByUsername(username);
		if(user == null)
			throw new UsernameNotFoundException("Invalid username or password.");
		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), getAuthority(user.getIsAdmin()));
	}

	public List<SimpleGrantedAuthority> getAuthority(String username) throws UsernameNotFoundException {
		User user = userRepository.findByUsername(username);
		if(user == null)
			throw new UsernameNotFoundException("Invalid username or password.");
		return getAuthority(user.getIsAdmin());
	}

	public List<SimpleGrantedAuthority> getAuthority(Boolean isAdmin) {
		if(isAdmin)
			return Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"), new SimpleGrantedAuthority("ROLE_USER"));
		else
			return Arrays.asList(new SimpleGrantedAuthority("ROLE_USER"));
	}

	public User findOne(String username) {
		return userRepository.findByUsername(username);
	}

    public User register(UserDetailsDto userDetailsDto) {
		User user = new User();
		user.setUsername(userDetailsDto.getUsername());
		user.setPassword(bcryptEncoder.encode(new String(Base64.decodeBase64(userDetailsDto.getPassword()))));
		user.setIsAdmin(false);
		user.setSurname(userDetailsDto.getSurname());
		user.setFirstname(userDetailsDto.getFirstname());
		user.setMail(userDetailsDto.getMail());
		user.setPhone(userDetailsDto.getPhone());
		Wallet wallet = new Wallet();
		wallet.setBalance(0);
		wallet.setUser(user);
		Cart cart = new Cart();
		cart.setStatus(0);
		cart.setUser(user);
		userRepository.save(user);
		cartRepository.save(cart);
		walletRepository.save(wallet);
        return user;
    }

	public User update(User user, UserDetailsDto userDetailsDto) {
		user.setPassword(bcryptEncoder.encode(new String(Base64.decodeBase64(userDetailsDto.getPassword()))));
		user.setSurname(userDetailsDto.getSurname());
		user.setFirstname(userDetailsDto.getFirstname());
		user.setMail(userDetailsDto.getMail());
		user.setPhone(userDetailsDto.getPhone());
		userRepository.save(user);
		return user;
	}
}
