package pl.newbuy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.newbuy.domain.Cart;
import pl.newbuy.domain.OrderItem;
import pl.newbuy.domain.Product;
import pl.newbuy.dto.*;
import pl.newbuy.repository.PurchaseRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import static java.lang.Math.toIntExact;

@Component
public class CartService {
	@Autowired
	private PurchaseService purchaseService;
	@Autowired
	private PurchaseRepository purchaseRepository;

	public CartDto createCartDto(Cart cart){
		CartDto cartDto = new CartDto();
		Map<Long, List<OrderItemDto>> groupedOrders = cart.getOrders().stream().map(order -> {
			OrderItemDto orderItemDto = new OrderItemDto();
			orderItemDto.setAmount(order.getAmount());
			orderItemDto.setQuantity(order.getQuantity());
			orderItemDto.setName(order.getPurchase().getProduct().getName());
			orderItemDto.setProductId(order.getPurchase().getProduct().getId());
			orderItemDto.setId(order.getId());
			return orderItemDto;
		}).collect(Collectors.groupingBy(OrderItemDto::getProductId, Collectors.toList()));
		cartDto.setItems(groupedOrders.entrySet().stream().map(entry -> {
			List<OrderItemDto> orderItemDtos = entry.getValue();
			OrderItemDto orderItemDto = new OrderItemDto();
			orderItemDto.setProductId(orderItemDtos.get(0).getProductId());
			orderItemDto.setName(orderItemDtos.get(0).getName());
			orderItemDto.setId(orderItemDtos.get(0).getId());
			orderItemDto.setQuantity(toIntExact(Math.round(orderItemDtos.stream().mapToDouble(OrderItemDto::getQuantity).sum())));
			orderItemDto.setAmount(toIntExact(Math.round(orderItemDtos.stream().mapToDouble(OrderItemDto::getAmount).sum())));
			return orderItemDto;
		}).collect(Collectors.toList()));
		cartDto.setDate(cart.getOrderDate());
		cartDto.setUsername(cart.getUser().getUsername());
		return cartDto;
	}

	public List<OrderDto> toOrderDtos(List<Cart> cartList){
		return cartList.stream().map(cart -> {
			int totalAmount = toIntExact(Math.round(cart.getOrders().stream().mapToDouble(order -> order.getQuantity() * order.getAmount()).sum()));
			int totalQuantity = toIntExact(Math.round(cart.getOrders().stream().mapToDouble(OrderItem::getQuantity).sum()));
			OrderDto orderDto = new OrderDto();
			orderDto.setId(cart.getId());
			orderDto.setProductsCount(cart.getOrders().size());
			orderDto.setTotalAmount(totalAmount);
			orderDto.setTotalQuantity(totalQuantity);
			orderDto.setDate(cart.getOrderDate());
			orderDto.setUsername(cart.getUser().getUsername());
			return orderDto;
		}).collect(Collectors.toCollection(ArrayList::new));
	}

	public List<OrderItem> createOrderItems(CartItemDto cartItemDto, Product product, Cart cart) {
		List<SelectedPurchaseDto> selectedPurchaseDtos = purchaseService.orderFromOldestPurchases(product.getId(), cartItemDto.getQuantity());
		return selectedPurchaseDtos.stream().map(selectedPurchaseDto -> {
			OrderItem orderItem = new OrderItem();
			orderItem.setPurchase(purchaseRepository.findOne(selectedPurchaseDto.getPurchaseId()));
			orderItem.setCart(cart);
			orderItem.setQuantity(selectedPurchaseDto.getQuantity());
			orderItem.setAmount(product.getAmount());
			return orderItem;
		}).collect(Collectors.toList());
	}
}
