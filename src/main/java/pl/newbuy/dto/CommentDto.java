package pl.newbuy.dto;

public class CommentDto {

    private Long id;
    private String content;
    private String username;

    public CommentDto(Long commentId, String content, String username){
        this.id = commentId;
        this.content = content;
        this.username = username;
    }
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
