package pl.newbuy.dto;

import java.util.Date;
import java.util.List;

public class CartDto {

    private List<OrderItemDto> items;
    private Date date;
    private String username;

    public List<OrderItemDto> getItems() {
        return items;
    }

    public void setItems(List<OrderItemDto> items) {
        this.items = items;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
