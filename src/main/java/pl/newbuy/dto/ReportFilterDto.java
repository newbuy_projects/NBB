package pl.newbuy.dto;


public class ReportFilterDto {

    private DateRangeDto dateRangeDto;
    private int productId;

    public DateRangeDto getDateRangeDto() {
        return dateRangeDto;
    }

    public void setDateRangeDto(DateRangeDto dateRangeDto) {
        this.dateRangeDto = dateRangeDto;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }
}
