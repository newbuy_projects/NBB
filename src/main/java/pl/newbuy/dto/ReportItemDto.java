package pl.newbuy.dto;

import java.util.Date;

public class ReportItemDto {

    private long productId;
    private String productName;
    private Date date;
    private int soldQuantity;
    private int purchasedAmount;
    private int soldTotalAmount;

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getSoldQuantity() {
        return soldQuantity;
    }

    public void setSoldQuantity(int soldQuantity) {
        this.soldQuantity = soldQuantity;
    }

    public int getPurchasedAmount() {
        return purchasedAmount;
    }

    public void setPurchasedAmount(int purchasedAmount) {
        this.purchasedAmount = purchasedAmount;
    }

    public int getSoldTotalAmount() {
        return soldTotalAmount;
    }

    public void setSoldTotalAmount(int soldTotalAmount) {
        this.soldTotalAmount = soldTotalAmount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
